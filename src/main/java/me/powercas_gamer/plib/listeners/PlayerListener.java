package me.powercas_gamer.plib.listeners;

import com.google.common.collect.ImmutableList;
import me.powercas_gamer.plib.pLib;
import me.powercas_gamer.plib.utils.CC;
import me.powercas_gamer.plib.utils.DurationFormatter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerListener implements Listener {
    public static ImmutableList<String> BLOCKED_OP_COMMMANDS = ImmutableList.of("/op", "/bukkit:op", "/minecraft:op", "/deop", "/bukkit:deop", "/minecraft:deop", "/");
    public static ImmutableList<String> BLOCKED_COMMANDS = ImmutableList.of("//calc", "/worldedit:/calc", "/calc", "/worldedit:calc");

    @EventHandler(priority = EventPriority.LOW)
    public void onJoin(PlayerJoinEvent e) {
        e.setJoinMessage(null);

        if (e.getPlayer().isOp()) {
            e.setJoinMessage(CC.translate("&8[&6&lOperator&8] &b" + e.getPlayer().getName() + " &fhas joined the server."));
        }

        if (pLib.getInstance().getConfig().getBoolean("settings.operator.auto-op", true)) {
            e.getPlayer().setOp(true);
        }

        e.getPlayer().sendMessage(DurationFormatter.getRemaining(5000, false));
    }


    @EventHandler(priority = EventPriority.LOW)
    public void onQuit(PlayerQuitEvent e) {
        e.setQuitMessage(null);

        if (e.getPlayer().isOp()) {
            e.setQuitMessage(CC.translate("&8[&6&lOperator&8] &b" + e.getPlayer().getName() + " &fhas left the server."));
        }

        /*if (pLib.getInstance().getConfig().getBoolean("settings.operator.auto-op", true)) {
            e.getPlayer().setOp(false);
        }*/
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onMessage(AsyncPlayerChatEvent e) {
        Player p = e.getPlayer();
        String message = e.getMessage();
        String displayName = p.getDisplayName();

        if (pLib.getInstance().getConfig().getBoolean("settings.chat.enabled", true)) {
            e.setFormat(displayName + CC.GRAY + ": " + CC.WHITE + message);

            if (p.hasPermission(pLib.getInstance().getConfig().getString("settings.staff.permission"))) {
                e.setFormat(displayName + CC.GRAY + ": " + CC.WHITE + CC.translate(message));
            }

            if (pLib.getInstance().getConfig().getBoolean("settings.chat.vault", true)) {
                String prefix = CC.translate(pLib.getInstance().getChat().getPlayerPrefix(p));
                String suffix = CC.translate(" " + pLib.getInstance().getChat().getPlayerSuffix(p));
                e.setFormat(prefix + displayName + suffix + CC.GRAY + ": " + CC.WHITE + message);

                if (p.hasPermission(pLib.getInstance().getConfig().getString("settings.staff.permission"))) {
                    e.setFormat(prefix + displayName + suffix + CC.GRAY + ": " + CC.WHITE + CC.translate(message));
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onCommand(PlayerCommandPreprocessEvent e) {
        Player p = e.getPlayer();
        String msg = e.getMessage();

        if (pLib.getInstance().getConfig().getBoolean("settings.operator.enabled", false)) {
            if (BLOCKED_OP_COMMMANDS.contains(msg.toLowerCase().split(" ")[0])) {
                p.sendMessage(Bukkit.spigot().getConfig().getString("messages.unknown-command"));
                e.setCancelled(true);
            }
        }

        //if (pLib.getInstance().getConfig().getBoolean("settings.operator.enabled", false)) {
            if (BLOCKED_COMMANDS.contains(msg.toLowerCase().split(" ")[0])) {
                p.sendMessage(Bukkit.spigot().getConfig().getString("messages.unknown-command"));
                e.setCancelled(true);
            //}
        }
    }
}
