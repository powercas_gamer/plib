package me.powercas_gamer.plib.utils;

import org.bukkit.potion.PotionEffectType;

public class PotionUtil {

    public static String getName(PotionEffectType potionEffectType) {
        if (potionEffectType.getName().equalsIgnoreCase("fire_resistance")) {
            return "Fire Resistance";
        } else if (potionEffectType.getName().equalsIgnoreCase("speed")) {
            return "Speed";
        } else if (potionEffectType.getName().equalsIgnoreCase("weakness")) {
            return "Weakness";
        } else if (potionEffectType.getName().equalsIgnoreCase("slowness")) {
            return "Slowness";
        } else if (potionEffectType.getName().equalsIgnoreCase("fast_digging")) {
            return "Haste";
        } else if (potionEffectType.getName().equalsIgnoreCase("night_vision")) {
            return "Night Vision";
        } else if (potionEffectType.getName().equalsIgnoreCase("jump")) {
            return "Jump Boost";
        } else {
            return "Unknown";
        }
    }
}
