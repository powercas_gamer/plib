package me.powercas_gamer.plib.utils;

import me.powercas_gamer.plib.pLib;
import org.bukkit.ChatColor;

import java.util.ArrayList;
import java.util.List;

public class CC {

    public static String BLUE = null;
    public static String AQUA = null;
    public static String YELLOW = null;
    public static String RED = null;
    public static String GRAY = null;
    public static String GOLD = null;
    public static String GREEN = null;
    public static String WHITE = null;
    public static String BLACK = null;
    public static String BOLD = null;
    public static String ITALIC = null;
    public static String UNDER_LINE = null;
    public static String STRIKE_THROUGH = null;
    public static String RESET = null;
    public static String MAGIC = null;
    public static String DARK_BLUE = null;
    public static String DARK_AQUA = null;
    public static String DARK_GRAY = null;
    public static String DARK_GREEN = null;
    public static String DARK_PURPLE = null;
    public static String DARK_RED = null;
    public static String PINK = null;
    public static String MENU_BAR = null;
    public static String CHAT_BAR = null;
    public static String SB_BAR = null;
    public static String PLAYER_NOT_FOUND = null;
    public static String PLAYER_ONLY = null;
    public static String NO_PERMS = null;



    static {
        try {
            BLUE = ChatColor.BLUE.toString();
            AQUA = ChatColor.AQUA.toString();
            YELLOW = ChatColor.YELLOW.toString();
            RED = ChatColor.RED.toString();
            GRAY = ChatColor.GRAY.toString();
            GOLD = ChatColor.GOLD.toString();
            GREEN = ChatColor.GREEN.toString();
            WHITE = ChatColor.WHITE.toString();
            BLACK = ChatColor.BLACK.toString();
            BOLD = ChatColor.BOLD.toString();
            ITALIC = ChatColor.ITALIC.toString();
            UNDER_LINE = ChatColor.UNDERLINE.toString();
            STRIKE_THROUGH = ChatColor.STRIKETHROUGH.toString();
            RESET = ChatColor.RESET.toString();
            MAGIC = ChatColor.MAGIC.toString();
            DARK_BLUE = ChatColor.DARK_BLUE.toString();
            DARK_AQUA = ChatColor.DARK_AQUA.toString();
            DARK_GRAY = ChatColor.DARK_GRAY.toString();
            DARK_GREEN = ChatColor.DARK_GREEN.toString();
            DARK_PURPLE = ChatColor.DARK_PURPLE.toString();
            DARK_RED = ChatColor.DARK_RED.toString();
            PINK = ChatColor.LIGHT_PURPLE.toString();
            MENU_BAR = ChatColor.GRAY.toString() + ChatColor.STRIKETHROUGH.toString() + "------------------------";
            CHAT_BAR = ChatColor.GRAY.toString() + ChatColor.STRIKETHROUGH.toString() + "------------------------------------------------";
            SB_BAR = ChatColor.GRAY.toString() + ChatColor.STRIKETHROUGH.toString() + "----------------------";
            PLAYER_NOT_FOUND = translate(pLib.getPlugin().getConfig().getString("messages.errors.player-not-found"));
            PLAYER_ONLY = translate(pLib.getPlugin().getConfig().getString("messages.errors.player-only"));
            NO_PERMS = translate(pLib.getPlugin().getConfig().getString("messages.errors.no-permission"));

        } catch(Exception e) {
            e.printStackTrace();

        }
    }

    public static String translate(String in) {
        return ChatColor.translateAlternateColorCodes('&', in);
    }

    public static List<String> translate(List<String> lines) {
        List<String> toReturn = new ArrayList<>();

        for (String line : lines) {
            toReturn.add(ChatColor.translateAlternateColorCodes('&', line));
        }
        return toReturn;
    }

    public static List<String> translate(String[] lines) {
        List<String> toReturn = new ArrayList<>();

        for (String line : lines) {
            if (line != null) {
                toReturn.add(ChatColor.translateAlternateColorCodes('&', line));
            }
        }
        return toReturn;
    }
}
