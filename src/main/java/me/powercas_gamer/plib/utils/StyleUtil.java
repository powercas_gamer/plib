package me.powercas_gamer.plib.utils;

import org.apache.commons.lang.StringEscapeUtils;

public class StyleUtil {
    private double tps;

    public static String colorPing(int ping) {
        if (ping <= 40) {
            return CC.GREEN + ping;
        } else if (ping <= 70) {
            return CC.YELLOW + ping;
        } else if (ping <= 100) {
            return CC.GOLD + ping;
        } else {
            return CC.RED + ping;
        }
    }


    public static String colorHealth(double health) {
        if (health > 15) {
            return CC.GREEN + convertHealth(health);
        } else if (health > 10) {
            return CC.GOLD + convertHealth(health);
        } else if (health > 5) {
            return CC.YELLOW + convertHealth(health);
        } else {
            return CC.RED + convertHealth(health);
        }
    }

    public double getTPS() {
        return this.tps + 1.0D > 20.0D ? 20.0D : this.tps + 1.0D;
    }

    public String colorTps() {
        if(this.tps >= 17) {
            return "§a" + this.tps;
        }
        else if(this.tps >= 15 && tps < 17) {
            return "§e" + this.tps;
        }
        else if(this.tps >= 10 && tps < 15) {
            return "§6" + this.tps;
        }
        else if(this.tps >= 5 && tps < 10) {
            return "§c" + this.tps;
        }
        else if(this.tps > 0 && tps < 5) {
            return "§4" + this.tps;
        }
        else {
            return "§e" + this.tps;
        }
    }


    public static double convertHealth(double health) {
        double dividedHealth = health / 2;

        if (dividedHealth % 1 == 0) {
            return dividedHealth;
        }

        if (dividedHealth % .5 == 0) {
            return dividedHealth;
        }

        if (dividedHealth - ((int) dividedHealth) > .5) {
            return ((int) dividedHealth) + 1;
        } else if (dividedHealth - ((int) dividedHealth) > .25) {
            return ((int) dividedHealth) + .5;
        } else {
            return ((int) dividedHealth);
        }
    }



    public static String getHeartIcon() {
        return StringEscapeUtils.unescapeJava("\u2764");
    }



}