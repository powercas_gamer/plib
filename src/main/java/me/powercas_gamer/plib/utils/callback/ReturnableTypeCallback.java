package me.powercas_gamer.plib.utils.callback;

public interface ReturnableTypeCallback<T> {

	T call();

}
