package me.powercas_gamer.plib.commands;

import me.powercas_gamer.plib.utils.CC;
import me.powercas_gamer.plib.utils.DurationFormatter;
import me.powercas_gamer.plib.utils.TimeUtil;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class TestCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        sender.sendMessage(CC.CHAT_BAR);
        sender.sendMessage(DurationFormatter.getRemaining(5000, false));
        sender.sendMessage(TimeUtil.millisToSeconds(50000));
        sender.sendMessage(CC.CHAT_BAR);
        return true;
    }
}
