package me.powercas_gamer.plib;

import io.papermc.lib.PaperLib;
import lombok.Getter;
import me.powercas_gamer.plib.hooks.LuckPermsImpl;
import me.powercas_gamer.plib.hooks.VaultImpl;
import me.powercas_gamer.plib.listeners.PlayerListener;
import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

public final class pLib extends JavaPlugin {
    @Getter
    public static File conf;
    public static FileConfiguration config;
    public static pLib plugin;
    public static pLib instance;
    @Getter private Chat chat = null;
    @Getter private Permission permission = null;
    @Getter private VaultImpl vaultImpl;
    @Getter private LuckPermsImpl luckPermsImpl;

    @Override
    public void onLoad() {
        pLib.plugin = this;
        pLib.instance = this;
        PaperLib.suggestPaper(this);
    }

    @Override
    public void onEnable() {
        registerConfiguration();
        registerManagers();
        //setupChat();
        //setupPermission();
        registerListeners();

        if (vaultImpl.isHooked()) {
            System.out.println("[pLib] Enabled Vault Implementation.");
        }

        if (luckPermsImpl.isHooked()) {
            System.out.println("[pLib] Enabled LuckPerms Implementation.");
        }
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    private void registerConfiguration() {
        config = getConfig();
        config.options().copyDefaults(true);
        saveConfig();
        conf = new File(getDataFolder(), "config.yml");
    }



    public static pLib getPlugin() {
        return plugin;
    }

    public static pLib getInstance() {
        return instance;
    }

    /*private boolean setupChat() {
        RegisteredServiceProvider<Chat> chatProvider =  this.getServer().getServicesManager().getRegistration(Chat.class);
        if (chatProvider != null) {
            chat = chatProvider.getProvider();
        }

        return (chat != null);
    }

    private boolean setupPermission() {
        RegisteredServiceProvider<Permission> permissionProvider = this.getServer().getServicesManager().getRegistration(Permission.class);
        if (permissionProvider != null) {
            permission = permissionProvider.getProvider();
        }

        return (permission != null);
    }*/

    public void registerManagers() {
        vaultImpl = new VaultImpl(this);
        luckPermsImpl = new LuckPermsImpl(this);
    }

    private void registerListeners() {
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new PlayerListener(), this);
    }

}