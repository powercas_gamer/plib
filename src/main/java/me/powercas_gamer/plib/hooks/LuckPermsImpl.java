package me.powercas_gamer.plib.hooks;

import lombok.Getter;
import me.powercas_gamer.plib.pLib;
import net.luckperms.api.LuckPerms;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.RegisteredServiceProvider;

public class LuckPermsImpl {

    @Getter
    private boolean hooked;
    @Getter private LuckPerms api;

    public LuckPermsImpl(pLib plugin) {
        Plugin luckPermsPlugin;

        if ((luckPermsPlugin = plugin.getServer().getPluginManager().getPlugin("LuckPerms")) == null || !luckPermsPlugin.isEnabled()) {
            return;
        }

        RegisteredServiceProvider<LuckPerms> provider = Bukkit.getServicesManager().getRegistration(LuckPerms.class);
        if (provider != null) {
            LuckPerms api = provider.getProvider();

        }

        hooked = true;
    }

    public LuckPerms getApi() {
        if (!hooked) {
            Plugin luckPermsPlugin;

            if((luckPermsPlugin = pLib.getPlugin().getServer().getPluginManager().getPlugin("LuckPerms")) == null || !luckPermsPlugin.isEnabled()) {
                throw new RuntimeException();
            }

            RegisteredServiceProvider<LuckPerms> provider = Bukkit.getServicesManager().getRegistration(LuckPerms.class);
            if (provider != null) {
                LuckPerms api = provider.getProvider();

            }

            assert provider != null;
            api = provider.getProvider();

            hooked = true;
        }

        return api;
    }
}

