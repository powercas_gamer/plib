package me.powercas_gamer.plib.hooks;

import lombok.Getter;
import me.powercas_gamer.plib.pLib;
import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.RegisteredServiceProvider;

import static org.bukkit.Bukkit.getServer;

public class VaultImpl {

    @Getter
    private boolean hooked;
    @Getter
    private Chat chat;
    @Getter
    private Permission permission;

    public VaultImpl(pLib plugin) {
        Plugin vaultPlugin;

        if((vaultPlugin = plugin.getServer().getPluginManager().getPlugin("Vault")) == null || !vaultPlugin.isEnabled()) {
            return;
        }


        RegisteredServiceProvider<Chat> chatRegisteredServiceProvider = plugin.getServer().getServicesManager().getRegistration(Chat.class);
        if(chatRegisteredServiceProvider == null) {
            return;
        }

        chat = chatRegisteredServiceProvider.getProvider();
        setupChat();
        setupPermission();

        hooked = true;
    }



    public Chat getChat() {
        if(!hooked) {
            Plugin vaultPlugin;

            if((vaultPlugin = pLib.getPlugin().getServer().getPluginManager().getPlugin("Vault")) == null || !vaultPlugin.isEnabled()) {
                throw new RuntimeException();
            }

            RegisteredServiceProvider<Chat> chatRegisteredServiceProvider = pLib.getPlugin().getServer().getServicesManager().getRegistration(Chat.class);
            if(chatRegisteredServiceProvider == null) {
                throw new RuntimeException();
            }

            chat = chatRegisteredServiceProvider.getProvider();

            hooked = true;
        }

        return chat;
    }

    public Permission getPermission() {
        if (!hooked) {
            Plugin vaultPlugin;

            if ((vaultPlugin = pLib.getPlugin().getServer().getPluginManager().getPlugin("Vault")) == null || !vaultPlugin.isEnabled()) {
                throw new RuntimeException();
            }

            RegisteredServiceProvider<Permission> permissionRegisteredServiceProvider = pLib.getPlugin().getServer().getServicesManager().getRegistration(Permission.class);
            if (permissionRegisteredServiceProvider == null) {
                throw new RuntimeException();
            }

            permission = permissionRegisteredServiceProvider.getProvider();

            hooked = true;
        }

        return permission;
    }

    private boolean setupChat() {
        RegisteredServiceProvider<Chat> chatProvider =  getServer().getServicesManager().getRegistration(Chat.class);
        if (chatProvider != null) {
            chat = chatProvider.getProvider();
        }

        return (chat != null);
    }

    private boolean setupPermission() {
        RegisteredServiceProvider<Permission> permissionProvider = getServer().getServicesManager().getRegistration(Permission.class);
        if (permissionProvider != null) {
            permission = permissionProvider.getProvider();
        }

        return (permission != null);
    }
}
