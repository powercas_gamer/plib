package me.powercas_gamer.atom.utils.menu.button;

import java.util.Arrays;
import lombok.AllArgsConstructor;
import me.powercas_gamer.stark.utils.CC;
import me.powercas_gamer.stark.utils.ItemBuilder;
import me.powercas_gamer.stark.utils.menu.Button;
import me.powercas_gamer.stark.utils.menu.Menu;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

@AllArgsConstructor
public class BackButton extends Button {

	private Menu back;

	@Override
	public ItemStack getButtonItem(Player player) {
		return new ItemBuilder(Material.REDSTONE)
				.name(CC.RED + CC.BOLD + "Back")
				.lore(Arrays.asList(
						CC.RED + "Click here to return to",
						CC.RED + "the previous menu.")
				)
				.build();
	}

	@Override
	public void clicked(Player player, ClickType clickType) {
		Button.playNeutral(player);
		back.openMenu(player);
	}

}
